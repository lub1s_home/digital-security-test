<?php

return [
    'group.destroy' => 'Group was deleted',
    'group.store' => 'Group was created',
    'group.update' => 'Group was updated',
    'group_user.destroy' => 'User has been removed from the group',
    'group_user.user_not_in_group' => 'User is not in group',
    'invite.accept' => 'You have joined the group',
    'invite.destroy' => 'Invite was deleted',
    'invite.no_invitation' => 'No invitation',
    'invite.store' => 'User was invited',
    'invite.user_in_group' => 'User is already a member of the group',
    'invite.user_already_invited' => 'The user is already invited',
    'forbidden' => 'Action is forbidden',
    'user.register' => 'User was created',
    'user.unauthorized' => 'Unauthenticated',
];
