<?php

namespace App\Providers;

use App\Group;
use App\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('action-with-group', function (User $user, Group $group) {
            return $user->id === $group->created_by_user_id;
        });
        Gate::define('delete-group-user', function (User $user, User $removableUser) {
            return $user->id !== $removableUser->id;
        });
    }
}
