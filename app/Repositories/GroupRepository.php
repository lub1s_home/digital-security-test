<?php

namespace App\Repositories;

use App\Group;

class GroupRepository
{
    /**
     * @var Group
     */
    private $model;

    public function __construct(Group $groupModel)
    {
        $this->model = $groupModel;
    }

    /**
     * Получить группы, в которых состоит пользователь, а также всех пользователей этих групп.
     *
     * @param int $userId
     * @return array
     */
    public function getUserGroupsAndUsersAsArray(int $userId): array
    {
        $userGroups = $this->model
            ->select(['groups.id'])
            ->joinToUsers()
            ->where('users.id', $userId)
            ->get()
            ->toArray();
        $groupIds = array_column($userGroups, 'id');

        $userGroupsAndUsers = $this->model
            ->select([
                'groups.id', 'groups.name', 'groups.created_by_user_id', 'groups.created_at', 'groups.updated_at',
                'users.id as u_id', 'users.name as u_name', 'users.email', 'users.email_verified_at',
                'users.created_at as u_created_at', 'users.updated_at as u_updated_at',
            ])
            ->joinToUsers()
            ->whereIn('groups.id', $groupIds)
            ->get();

        $res = [];
        foreach ($userGroupsAndUsers as $group) {
            if (!isset($res[$group->id])) {
                $res[$group->id] = [
                    'id' => $group->id,
                    'name' => $group->name,
                    'created_by_user_id' => $group->created_by_user_id,
                    'created_at' => $group->created_at,
                    'updated_at' => $group->updated_at,
                ];
            }
            $res[$group->id]['users'][] = [
                'id' => $group->u_id,
                'name' => $group->u_name,
                'email' => $group->email,
                'email_verified_at' => $group->email_verified_at,
                'created_at' => $group->u_created_at,
                'updated_at' => $group->u_updated_at,
            ];
        }

        return $res;
    }

    /**
     * @param int $userId
     * @param int $groupId
     * @return bool
     */
    public function isUserInGroup(int $userId, int $groupId): bool
    {
        return $this->model->join('group_user', 'group_user.group_id', '=', 'groups.id')
            ->where('group_user.user_id', $userId)
            ->where('group_user.group_id', $groupId)
            ->exists();
    }
}
