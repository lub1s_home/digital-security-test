<?php

namespace App\Repositories;

use App\User;

class UserRepository
{
    /**
     * @var User
     */
    private $model;

    public function __construct(User $userModel)
    {
        $this->model = $userModel;
    }

    /**
     * @param string $email
     * @return User
     */
    public function findByEmailOrFail(string $email): User
    {
        return $this->model->where('email', $email)->firstOrFail();
    }
}
