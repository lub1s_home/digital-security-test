<?php

namespace App\Repositories;

use App\Invite;
use Illuminate\Database\Eloquent\Collection;

class InviteRepository
{
    /**
     * @var Invite
     */
    private $model;

    public function __construct(Invite $inviteModel)
    {
        $this->model = $inviteModel;
    }

    /**
     * @param int $userId
     * @param int $groupId
     * @return Invite|null
     */
    public function getInvite(int $userId, int $groupId): ?Invite
    {
        return $this->model->where('user_id', $userId)
            ->where('group_id', $groupId)
            ->first();
    }

    /**
     * @param int $userId
     * @param int $groupId
     * @return bool
     */
    public function isUserInvited(int $userId, int $groupId): bool
    {
        return $this->model->notConfirmed($userId, $groupId)
            ->exists();
    }

    /**
     * @param int $userId
     * @param int $groupId
     * @return Invite|null
     */
    public function getNotConfirmed(int $userId, int $groupId): ?Invite
    {
        return $this->model->notConfirmed($userId, $groupId)
            ->first();
    }

    /**
     * Получить список приглашений для группы.
     *
     * @param int $groupId
     * @return Collection
     */
    public function getListWithUsersByGroupId(int $groupId): Collection
    {
        $invitesAndUsers = $this->model
            ->select([
                'invites.id', 'invites.user_id', 'invites.group_id', 'invites.created_at', 'invites.updated_at',
                'invites.is_accepted',
                'users.id as u_id', 'users.name as u_name', 'users.email', 'users.email_verified_at',
                'users.created_at as u_created_at', 'users.updated_at as u_updated_at',
            ])
            ->join('users', 'users.id', '=', 'invites.user_id')
            ->where('invites.group_id', $groupId)
            ->get();

        foreach ($invitesAndUsers as &$invite) {
            $invite->user = [
                'id' => $invite->u_id,
                'name' => $invite->u_name,
                'email' => $invite->email,
                'email_verified_at' => $invite->email_verified_at,
                'created_at' => $invite->u_created_at,
                'updated_at' => $invite->u_updated_at,
            ];
            unset(
                $invite->u_id,
                $invite->u_name,
                $invite->email,
                $invite->email_verified_at,
                $invite->u_created_at,
                $invite->u_updated_at,
            );
        }
        unset($invite);

        return $invitesAndUsers;
    }
}
