<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Invite extends Model
{
    protected $guarded = [];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function group(): BelongsTo
    {
        return $this->belongsTo(Group::class);
    }

    public function scopeNotConfirmed(Builder $query, int $userId, int $groupId): Builder
    {
        return $query->where('user_id', $userId)
            ->where('group_id', $groupId)
            ->where('is_accepted', false);
    }
}
