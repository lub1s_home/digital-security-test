<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    protected const UNAUTHORIZED_SERVER_CODE = 401;
    protected const FORBIDDEN_SERVER_CODE = 403;
    protected const UNPROCESSABLE_ENTITY_SERVER_CODE = 422;
    protected const INTERNAL_SERVER_ERROR_SERVER_CODE = 500;

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
