<?php

namespace App\Http\Controllers;

use App\Group;
use App\Http\Requests\Groups\StoreRequest;
use App\Repositories\GroupRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class GroupController extends Controller
{
    /**
     * @var GroupRepository
     */
    private $groupRepo;

    public function __construct(GroupRepository $groupRepo)
    {
        $this->groupRepo = $groupRepo;
    }

    /**
     * Посмотреть все существующие группы,
     * а также пользователей только из тех групп, в которых состоит текущий пользователь.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $user = Auth::user();
        $allGroups = Group::all()->toArray();
        $userGroupsAndUsers = $this->groupRepo->getUserGroupsAndUsersAsArray($user->id);
        foreach ($allGroups as &$group) {
            if (isset($userGroupsAndUsers[$group['id']])) {
                $group = $userGroupsAndUsers[$group['id']];
            }
        }
        unset($group);

        return response()->json(['groups' => $allGroups]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return JsonResponse
     */
    public function store(StoreRequest $request): JsonResponse
    {
        $user = Auth::user();
        $group = new Group();
        $group->name = request('name');
        $group->created_by_user_id = $user->id;
        $group->save();
        $group->users()->attach($user->id);

        return response()->json([
            'message' => __('messages.group.store'),
            'group' => $group,
        ]);
    }

    /**
     * Посмотреть информацию о группе.
     * Посмотреть пользователей, если текущий пользователь состоит в этой группе.
     *
     * @param Group $group
     * @return JsonResponse
     */
    public function show(Group $group): JsonResponse
    {
        $user = Auth::user();
        $response = ['group' => $group];
        $groupUsers = $group->users()->get();
        $isUserInGroup = $groupUsers->contains('id', $user->id);
        if (!$isUserInGroup) {
            return response()->json($response);
        }
        $response['users'] = $groupUsers;

        return response()->json($response);

    }

    /**
     * Обновить группу, если пользователь является админом этой группы.
     *
     * @param Group $group
     * @param StoreRequest $request
     * @return JsonResponse
     */
    public function update(Group $group, StoreRequest $request): JsonResponse
    {
        Gate::authorize('action-with-group', $group);
        $group->update([
            'name' => request('name'),
        ]);

        return response()->json([
            'message' => __('messages.group.update'),
            'group' => $group,
        ]);
    }

    /**
     * Удалить группу, если пользователь является админом этой группы.
     *
     * @param Group $group
     * @return JsonResponse
     */
    public function destroy(Group $group): JsonResponse
    {
        Gate::authorize('action-with-group', $group);

        try {
            $group->delete();
        } catch (\Exception $e) {
            return response()->json(
                ['message' => $e->getMessage()],
                self::INTERNAL_SERVER_ERROR_SERVER_CODE
            );
        }

        return response()->json(['message' => __('messages.group.destroy')]);
    }
}
