<?php

namespace App\Http\Controllers;

use App\Group;
use App\Http\Requests\Invites\StoreRequest;
use App\Invite;
use App\Notifications\InvitedUser;
use App\Repositories\GroupRepository;
use App\Repositories\InviteRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class InviteController extends Controller
{
    /**
     * @var GroupRepository
     */
    private $groupRepo;

    /**
     * @var UserRepository
     */
    private $userRepo;

    /**
     * @var InviteRepository
     */
    private $inviteRepo;

    public function __construct(
        UserRepository $userRepo,
        GroupRepository $groupRepo,
        InviteRepository $inviteRepo
    ) {
        $this->userRepo = $userRepo;
        $this->groupRepo = $groupRepo;
        $this->inviteRepo = $inviteRepo;
    }

    /**
     * Принять приглашение в группу.
     *
     * @param Group $group
     * @return JsonResponse
     */
    public function accept(Group $group): JsonResponse
    {
        $user = Auth::user();
        $invite = $this->inviteRepo->getNotConfirmed($user->id, $group->id);
        if (!$invite) {
            return response()->json(
                ['message' => __('messages.invite.no_invitation')],
                self::UNPROCESSABLE_ENTITY_SERVER_CODE
            );
        }
        $invite->is_accepted = true;
        $invite->save();
        $group->users()->attach($user->id);

        return response()->json([
            'message' => __('messages.invite.accept'),
            'group' => $group,
        ]);
    }

    /**
     * Посмотреть приглашения для группы.
     *
     * @param Group $group
     * @return JsonResponse
     */
    public function index(Group $group): JsonResponse
    {
        Gate::authorize('action-with-group', $group);
        $invites = $this->inviteRepo->getListWithUsersByGroupId($group->id);

        return response()->json([
            'group' => $group,
            'invites' => $invites,
        ]);
    }

    /**
     * Пригласить пользователя в группу.
     *
     * @param StoreRequest $request
     * @param Group $group
     * @return JsonResponse
     */
    public function store(StoreRequest $request, Group $group): JsonResponse
    {
        Gate::authorize('action-with-group', $group);
        $invitedUser = $this->userRepo->findByEmailOrFail(request('email'));
        $isUserInGroup = $this->groupRepo->isUserInGroup($invitedUser->id, $group->id);
        if ($isUserInGroup) {
            return response()->json(
                ['message' => __('messages.invite.user_in_group')],
                self::UNPROCESSABLE_ENTITY_SERVER_CODE
            );
        }
        $isUserInvited = $this->inviteRepo->isUserInvited($invitedUser->id, $group->id);
        if ($isUserInvited) {
            return response()->json(
                ['message' => __('messages.invite.user_already_invited')],
                self::UNPROCESSABLE_ENTITY_SERVER_CODE
            );
        }
        $invite = new Invite([
            'user_id' => $invitedUser->id,
            'group_id' => $group->id,
        ]);
        $invite->save();
        $invitedUser->notify(new InvitedUser($invitedUser, $group));

        return response()->json([
            'message' => __('messages.invite.store'),
            'user' => $invitedUser,
            'group' => $group,
        ]);
    }

    /**
     * Посмотреть статус приглашения.
     *
     * @param Group $group
     * @param Invite $invite
     * @return JsonResponse
     */
    public function show(Group $group, Invite $invite): JsonResponse
    {
        Gate::authorize('action-with-group', $group);

        return response()->json(['invite' => $invite]);
    }

    /**
     * Удалить приглашение, если оно еще не принято.
     *
     * @param Group $group
     * @param Invite $invite
     * @return JsonResponse
     */
    public function destroy(Group $group, Invite $invite): JsonResponse
    {
        Gate::authorize('action-with-group', $group);
        if ($invite->is_accepted) {
            return response()->json(
                ['message' => __('messages.invite.user_in_group')],
                self::UNPROCESSABLE_ENTITY_SERVER_CODE
            );
        }

        try {
            $invite->delete();
        } catch (\Exception $e) {
            return response()->json(
                ['message' => $e->getMessage()],
                self::INTERNAL_SERVER_ERROR_SERVER_CODE
            );
        }

        return response()->json(['message' => __('messages.invite.destroy')]);
    }
}
