<?php

namespace App\Http\Controllers;

use App\Group;
use App\Repositories\InviteRepository;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Gate;

class GroupUserController extends Controller
{
    /**
     * @var InviteRepository
     */
    private $inviteRepo;

    public function __construct(InviteRepository $inviteRepo)
    {
        $this->inviteRepo = $inviteRepo;
    }

    /**
     * Удалить пользователя из группы.
     *
     * @param Group $group
     * @param User $user
     * @return JsonResponse
     */
    public function destroy(Group $group, User $user): JsonResponse
    {
        Gate::authorize('action-with-group', $group);
        Gate::authorize('delete-group-user', $user);
        $invite = $this->inviteRepo->getInvite($user->id, $group->id);
        if (!$invite || !$invite->is_accepted) {
            return response()->json(
                ['message' => __('messages.group_user.user_not_in_group')],
                self::UNPROCESSABLE_ENTITY_SERVER_CODE
            );
        }

        try {
            $invite->delete();
        } catch (\Exception $e) {
            return response()->json(
                ['message' => $e->getMessage()],
                self::INTERNAL_SERVER_ERROR_SERVER_CODE
            );
        }

        $user->groups()->detach($group->id);

        return response()->json([
            'message' => __('messages.group_user.destroy'),
            'user' => $user,
            'group' => $group,
        ]);
    }
}
