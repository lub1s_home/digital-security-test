<?php

namespace App\Http\Controllers;

use App\Http\Requests\Users\LoginRequest;
use App\Http\Requests\Users\RegisterRequest;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    private const TOKEN_NAME = 'default';

    /**
     * Регистрация пользователя.
     *
     * @param RegisterRequest $request
     * @return JsonResponse
     */
    public function register(RegisterRequest $request): JsonResponse
    {
        $user = User::create([
            'name' => request('name'),
            'email' => request('email'),
            'password' => Hash::make(request('password')),
        ]);

        return response()->json([
            'message' => __('messages.user.register'),
            'user' => $user,
        ]);
    }

    /**
     * Авторизация (получение токена).
     *
     * @param LoginRequest $request
     * @return JsonResponse
     */
    public function login(LoginRequest $request): JsonResponse
    {
        //Не выношу авторизацию в LoginRequest в метод authorize(), т.к. не будет срабатывать валидация в методе rules().

        $isAuthorized = Auth::attempt([
            'email' => request('email'),
            'password' => request('password'),
        ]);
        if (!$isAuthorized) {
            return response()->json(
                ['message' => __('messages.user.unauthorized')],
                self::UNAUTHORIZED_SERVER_CODE
            );
        }

        //TODO Как удалять токен, если он устарел, и делать новый?

        return response()->json([
            'token' => Auth::user()->createToken(self::TOKEN_NAME)->accessToken,
        ]);
    }
}
