<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'UserController@register');
Route::post('login', 'UserController@login');
Route::group(['middleware' => 'auth:api'], function() {
    Route::resource('groups', 'GroupController', ['except' => ['create', 'edit']]);
    Route::resource('groups/{group}/invites', 'InviteController', ['except' => ['create', 'edit', 'update']]);
    Route::post('groups/{group}/invites/accept', 'InviteController@accept')->name('invites.accept');
    Route::delete('groups/{group}/users/{user}', 'GroupUserController@destroy');
});
